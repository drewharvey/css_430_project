//======================================
// INODE CLASS
//=====================================
import java.util.*;


public class Inode {
    private final static int iNodeSize = 32;       // fix to 32 bytes
    private final static int directSize = 11;      // # direct pointers
	
    public int length;                             // file size in bytes
    public short count;                            // # file-table entries pointing to this
    public short flag;                             // 0 = unused, 1 = used, ...
    public short direct[] = new short[directSize]; // direct pointers
    public short indirect;                         // a indirect pointer

    public Inode( ) {                              // a default constructor
        length = 0;
        count = 0;
        flag = 1;
        for ( int i = 0; i < directSize; i++ )
           direct[i] = -1;
        indirect = -1;
    }
	  
	// default constructor that retrieves
	// inode from the disk
    public Inode( short iNumber ) {
        // do some setup of values
        int blockNumber = 1 + iNumber / 16;         // supernode + (requested number of blocks / nodes per block)
        byte[] data = new byte[Disk.blockSize];     
        SysLib.rawread(  blockNumber, data );       
        int offset = ( iNumber % 16) * 32;
        
        // read data from a disk. After every read, increment the offset so we can read from 
        // the next spot.
        length = SysLib.bytes2int( data, offset );
        offset += 4;
        count = SysLib.bytes2short( data, offset );
        offset += 2;
        flag = SysLib.bytes2short( data, offset );
        offset += 2;
        
        // read all the direct pointers
        for ( int i = 0; i < directSize; i++ ){
            direct[i] = SysLib.bytes2short( data, offset );
            offset += 2;
        }
        
        // read the indirect pointer
        indirect = SysLib.bytes2short( data, offset );
        offset += 2;
    }

    // save to disk as the i-th inode
    public int toDisk( short iNumber ) {       
        // do some setup of values      
        int blockNumber = 1 + (iNumber / 16);
        byte[] data = new byte[Disk.blockSize];
        SysLib.rawread(  blockNumber, data );
        int offset = ( iNumber % 16) * 32;
        
        // write length, count, and flag to data array.
        SysLib.int2bytes( length, data, offset );
        offset += 4;
        SysLib.short2bytes( count, data, offset );
        offset += 2;
        SysLib.short2bytes( flag, data, offset );
        offset += 2;
        
        // write direct pointers to data array.
        for ( int i = 0; i < directSize; i++ ){
            SysLib.short2bytes( direct[i], data, offset );
            offset += 2;
        }
        
        // write the indirect pointer to the data array.
        SysLib.short2bytes( indirect, data, offset );
        offset += 2;
        
        // still need to write data to the actual disk here.
        try {
            SysLib.rawwrite( blockNumber, data );
            return 0;
        }
        catch (Exception ex){
            //failed to write
        }
        
        return -1;
    }
}
