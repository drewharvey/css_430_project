import java.util.*;

public class FileTable {
    // holds file table entries. 
    private Vector<FileTableEntry> table;     // the actual entity of this file table
    private Directory dir;        // the root directory
    private final int MAX_FILES = 32; // or 29 ?

    public FileTable( Directory directory ) { // constructor
        table = new Vector<FileTableEntry>(MAX_FILES); 
        dir = directory;           // receive a reference to the Director
    }                             // from FileSystem

    // returns FileTableEntry 
    public synchronized FileTableEntry falloc( String filename, String mode ) {
        short iNumber = -1;
        Inode inode = null;
        
        iNumber = (filename.equals("/") ? 0 : dir.namei(filename));
        if(iNumber >= 0) { // the file exists
            inode = new Inode(iNumber); // read that iNode from DiSK!  
        }
        else { // the iNumber indicates the file doesn't exist
            if(mode.equals("r"))
                return null; // error
                
            // create the file in "w" "w+" and "a" since it doesn't exist
            inode = new Inode();         
                
            short regInodeInDir = dir.ialloc(filename);
            if(regInodeInDir == -1)
                return null; // couldn't allocate inode in Directory
        }
        
        
        inode.count++; //# file-table entries pointing to this Inode
        inode.toDisk(iNumber); //write to dis
        FileTableEntry e = new FileTableEntry(inode, iNumber, mode);
        table.addElement(e); // create a table entry and register it
        return e;   
    }
    
    // receive a file table entry reference
    public synchronized boolean ffree( FileTableEntry e ) {        
        // save the corresponding inode to the disk
        Inode inode = e.inode;
        if(inode.toDisk(e.iNumber) == -1)
            return false; // didn't write to the Disk
        
        for(int i = 0; i < table.size(); i++) {        
            if(table.elementAt(i).iNumber == e.iNumber) {
                inode.count--;
                if(inode.count == 0) 
                    inode.flag = 0; // no file entry is pointing to this inode
                
                table.removeElementAt(i); // free this file table entry.
                return true; //this file table entry found in my table
            } 
        }       
		return false;
    }

    public synchronized boolean fempty( ) {
        return table.isEmpty( );  // return if table is empty
    }// should be called before starting a format
}
