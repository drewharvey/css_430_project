//******************************************************************************
//**  File: QueueNode.java
//**  Date: February 2014
//**  Author: Dimitar Dimitrov
//**  Description: This is a QueueNode class which allows us to store and
//**  maniupulate java thread objects. Upon initialization of a new QueueNode
//**  object we are creating a deque(double ended queue). Moreover, the class
//**  allows us to put threads to sleep and wake them up.
//******************************************************************************
import java.util.*;

public class QueueNode{
	// deque container used to store the children tids of particular thread
	Deque<Integer> deque; 
	
	//default constructor
	public QueueNode() {
		deque = new ArrayDeque<Integer>();
	}

	// add the child thread to this particular parent/condition
	public void addThread(int tid) {		
		deque.add(tid);	 
	}
	
	// removes a child thread from the deque
	public void removeThread(int tid) {
		deque.remove(tid);	
	}
	
	// puts the calling thread to sleep and returns the tid woken up
	// the thread upon satisfying the condition. Upon return the thread
	//is removed from the deque.
	public synchronized int putToSleep() {
		try {
			while(deque.isEmpty()) { 
				wait(); //let other threads check their deques
			}				
		}catch(Exception e) {
			System.out.println(e);
		} // removes and returns the 1st child in the queue
		return deque.removeFirst();
	}
	
	// wakes up a parent thread by adding the thread to the deque
	// then the function notifies other child threads that they can
	// wake up the parent.
	public synchronized void wakeUp(int tid) {
		try {
			addThread(tid);
			notify(); // let other threads wake up their parent
		} catch(Exception e) {
			System.out.println(e);
		}
	}	
}
