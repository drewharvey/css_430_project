class SuperBlock {
    private final int defaultInodeBlocks = 64; // default inodes
    public int totalBlocks; // the number of disk blocks
    public int totalInodes; // the number of inodes
    public int freeList;    // the block number of the free list's head
	
	public SuperBlock(int diskSize) {
		// read the superblock from disk
		byte[] superBlock = new byte[Disk.blockSize]; //512 bytes
		SysLib.rawread(0, superBlock);
		totalBlocks = SysLib.bytes2int(superBlock,0); //1st time it passes all values = 0 
		totalInodes = SysLib.bytes2int(superBlock,4);
		freeList = SysLib.bytes2int(superBlock, 8);
		
		if(totalBlocks == diskSize && totalInodes > 0 && freeList >=2) {
			// disk contents are valid
			return;
		}
		else {
			// need to format disk
			System.out.println("Gets to SuperBlock Constructor");
			totalBlocks = diskSize;
			format(defaultInodeBlocks); // ??? On the slides it doesn't say SysLib.format
			// SuperBlock has a function format
		}		
	}	 
	
	// initialize the SuperBlock fields / "formats" the Disk
 	public boolean format(int files) {
		try{
			System.out.println("Gets to SuperBlock Format");
			totalInodes = files;
			freeList = (files / 16) + 1; // Don't count superBlock(at index 0)
			return true; //on success
		}
		catch(Exception e) {
			System.out.println(e);
		}	
		return false;
	}
}