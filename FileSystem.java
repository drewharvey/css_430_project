public class FileSystem {

    private SuperBlock superblock;
    private Directory directory;
    private FileTable filetable;
    
    // ------------- FOR TEST ONLY: REMOVE AFTER TESTED --------------//
    public FileSystem() {

    }
    // --------------------------------------------------------------//

    public FileSystem(int diskBlocks) {
    	superblock = new SuperBlock(diskBlocks); // 1000 blocks
    	directory = new Directory(superblock.totalInodes); // 64 by default
    	filetable = new FileTable(directory);
    }

    // This function is called from outside threads(User threads).
    // File system hasn't been initiated whne SuperBlock calls format function
    public boolean format( int files ) {
        // do we need to call FileTable.fempty() before format ? 
        return superblock.format(files); //call the superBlock's format
    }
    
    // Function which helps us return file descriptor to specific file
    public FileTableEntry open(String fileName, String mode) {   
        return filetable.falloc(fileName, mode); //return FileEntry 
    }
    
    public void sync() {
       	System.out.println("FileSystem Synchronize");
    }
    
    // raw version -- Might need to move this func to File Table class
    public int read(FileTableEntry ftEnt, byte[] buffer) {
        int blkNum = 0;
        byte[] data = new byte[Disk.blockSize];

        try {
            // find where(@ which block) the ptr is
            //blkNum = ftEnt.seekPtr / Disk.blockSize; 
            
            if (ftEnt.inode.indirect != -1) { // scan the indirect blk
                SysLib.rawread(ftEnt.inode.indirect, data); //read block
		
                for(int i = 0; i < Disk.blockSize; i += 2) { 
                    if(SysLib.bytes2short(data, i) == -1) { // i is the offset in this case
                        blkNum = --i; // last legit indirect block
                        break;       
	               }
                }
            }
            else { // parse the direct blocks and find the pointer
                for(int k = 0; k < 11; k++) {
                    if(k == -1) {
                        blkNum = --k; // last legit block index
                        break;
                    }
                }
            }

            //read the data from the block
            SysLib.rawread(blkNum, data);
            int index = 0;
            for(int j = ftEnt.seekPtr; j < Disk.blockSize; j++) {
                if( data[j] != 0) { // compare to null
                    buffer[index] = data[j];
                    index++;
                }         
            }
            return buffer.length; // return the length read
        }
        catch(Exception e) {
            System.out.println("Exception " + e);
        }
        
		return -1; //error
	}

	public int write(FileTableEntry ftEnt, byte[] buffer) {

        // check that params are all valid
        if (buffer.length < 1 || buffer == null || ftEnt == null)
            return -1;

        // find what block the seek ptr is in
        int curBlockNum = ftEnt.seekPtr / Disk.blockSize;
        int curBlockByte = ftEnt.seekPtr;

        // loop thru and write all the bytes in buffer
        for (int i = 0; i < buffer.length; i++) {

            // if we hit the end of our block, move to the next
            if (curBlockByte >= Disk.blockSize) {
                curBlockNum++;
            }

            // if we hit the end of our direct blocks, move to indirect blocks
            if (curBlockNum >= ftEnt.inode.direct.length) {

            }


        }

		return 0;
	}
    public int writeTest(byte[] buffer, byte[] blocks) {

        // check that params are all valid
        if (buffer.length < 1 || buffer == null)
            return -1;

        // find what block the seek ptr is in
        int curBlockNum = 0;                    // change
        int curBlockByte = 0;                   // change

        // loop thru and write all the bytes in buffer
        for (int i = 0; i < buffer.length; i++) {

            // if we hit the end of our block, move to the next
            if (curBlockByte >= Disk.blockSize) {
                curBlockNum++;
            }

            // if we hit the end of our direct blocks, move to indirect blocks
            if (curBlockNum >= blocks.length) {
                System.out.println("All direct blocks filled, should move to indirect");
                break;
            }

            // create a block sized buffer that we can pass to disk to use for writing to a block
            byte[] blockBuffer = new byte[Disk.blockSize];
            for (int j = 0; j < blockBuffer.length && i < buffer.length; j++) {
                blockBuffer[j] = buffer[i];
                i++;
            }
        }

        return 0;
    }

	public int seek(FileTableEntry ftEnt, int offset, int whence) {
		return 0;
	}
}