public class Directory {
    private static int maxFileNameSize = 30;
    private static int defaultMaxInodes = 64;

    // Directory entries
    private int fileSize[];
    private char fileName[][];


    public Directory() {
        this(defaultMaxInodes);
    }

    public Directory( int maxInodes ) {

        // make sure we have a valid maxInode number
        if (maxInodes < 1) {
            maxInodes = defaultMaxInodes;
        }

        // max number of files
        fileSize = new int[maxInodes];
        for ( int i = 0; i < maxInodes; i++ ) {
            fileSize[i] = 0;
        }

        // init file name arrays
        fileName = new char[maxInodes][maxFileNameSize];

        // update first inode (#0) to represent root file "/"
        String firstInodeFileName = "/";
        fileSize[0] = firstInodeFileName.length();
	
        // getChars(int srcBegin,int srcEnd,char[] dst, int dstBegin)

        // store inode #0's name in our fileName array
        firstInodeFileName.getChars(0, fileSize[0], fileName[0], 0);
    }
    
    public void bytes2directory( byte data[] ) {
        // assumes data[] received directory information from disk
        // initializes the Directory instance with this data[]
        int offset = 0;

        // convert bytes to file size entries
        for (int i = 0; i < fileSize.length; i++, offset += 4) {
            fileSize[i] = SysLib.bytes2int(data, offset);
        }

        // 1 char = 2 bytes
        int maxFileNameBytes = maxFileNameSize * 2;

        // convert bytes to file name entries
        for (int i = 0; i < fileName.length; i++, offset += maxFileNameBytes) {
            // String(byte[] bytes, int offset, int length)
            String fname = new String(data, offset, maxFileNameBytes); // NOTE: maybe instead of maxFileNameBytes, use fileSize[i] * 2.
            fname.getChars(0, fileSize[i], fileName[i], 0);
        }
    }
    
    public byte[] directory2bytes( ) {
        // converts and return Directory information into a plain byte array
        // this byte array will be written back to disk
        // note: only meaningfull directory information should be converted
        // into bytes.

        // 1 int = 4 bytes
        // 1 char = 2 bytes

        // calculate total num of bytes we need
        int numBytes = (fileSize.length * 4) 
            + (fileName.length * maxFileNameSize * 2);

        byte[] data = new byte[numBytes];

        // used to keep track of where in the data[] we are
        int offset = 0;

        // convert file size array to bytes
        for (int i = 0; i < fileSize.length; i++, offset += 4) {
            SysLib.int2bytes(fileSize[i], data, offset);
        }

        // 1 char = 2 bytes
        int maxFileNameBytes = maxFileNameSize * 2;

        // convert file name arrays to bytes
        for (int i = 0; i < fileName.length; i++, offset += maxFileNameBytes) { // <--- I think it's wrong should be fileSize[i - 1] * 2

            // convert the filename we have to a string
            String fname = new String(fileName[i], 0, fileSize[i]); // String(byte[] bytes, int offset, int length)

            // convert the new string filename to an array of bytes
            byte[] bytes = fname.getBytes();

            // copy the new array of bytes into our directory byte[]
            for (int j = 0; j < bytes.length; j++) {
                data[offset + j] = bytes[j];
            }
        }

        return data;
    }
    
    public short ialloc( String filename ) {
        // filename is the one of a file to be created.
        // allocates a new inode number for this filename

        if (filename.length() > maxFileNameSize)
            return -1;

        short inodeNum = findFirstEmptyInodeNumber();

        if (inodeNum < 0)
            return -1;

        fileSize[inodeNum] = filename.length();
        filename.getChars(0, fileSize[inodeNum], fileName[inodeNum], 0);

        return  inodeNum;
    }
    
    public boolean ifree( short inodeNum ) {
        // deallocates this inumber (inode number)
        // the corresponding file will be deleted.

        // iNode0 is reserved by default for the root "/"
        if (inodeNum <= 0 || inodeNum >= fileSize.length) 
            return false;

        if (fileSize[inodeNum] < 1)
            return true;

        fileSize[inodeNum] = 0;
        fileName[inodeNum] = new char[maxFileNameSize]; 

	   return true;
    }

    public short namei( String filename ) {
        // returns the inumber corresponding to this filename

        String existingFileName;

    	for (short i = 0; i < fileSize.length; i++) {
            // make sure we aren't including the empty chars in the comparison
            existingFileName = new String(fileName[i], 0, fileSize[i]);

    	    if (filename.equals(existingFileName)) {
                return i;
    	    }
    	}

    	return -1;
    }

    private short findFirstEmptyInodeNumber() {
        for (short i = 0; i < fileSize.length; i++) {
            if (fileSize[i] == 0) {
                return i;
            }
        }
        return -1;
    }

    //////////////////////////////////////////////////////////////////////////////
    //// TEST FUNCTIONS //////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////

    public void printFileInfo() {
        System.out.printf("%10s %10s %10s %n", "INODE #", "FILE SIZE", "FILE NAME");
        for (int i = 0; i < fileSize.length; i++) {
            System.out.printf("%10d %10d ", i, fileSize[i]);
            if (fileSize[i] > 0) {
                System.out.printf("%30s", new String(fileName[i]));
            }
            System.out.printf("%n");
        }
    }
}
