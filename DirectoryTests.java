import java.io.*;
import java.util.Scanner;

public class DirectoryTests {

    public static void main(String[] args) {
        // char[] chars = {'c', 'a', 't', 's'};
        // byte[] bytes = new String(chars).getBytes();

        // System.out.println(bytes.length);
        // for (int i = 0; i < bytes.length; i++)
        //     System.out.println(bytes[i]);

        // String s = new String(bytes, 0, bytes.length);
        // System.out.println(s);
        // if (true) return;

        DirectoryTests test = new DirectoryTests();
        test.Run();
    }



    int numInodes;
    Directory dir;

    public DirectoryTests() { }

    public void Run() {
        Scanner in = new Scanner(System.in);
        int input = 1;

        System.out.println("1: Add Files Test");
        System.out.println("2: Remove Files Test");
        System.out.println("3: Get Inode by Filename Test");
        System.out.println("4: Read/Write Directory Test");
        System.out.println("0: Exit");

        while (input != 0) {

            System.out.println("1: Add Files Test");
            System.out.println("2: Remove Files Test");
            System.out.println("3: Get Inode by Filename Test");
            System.out.println("4: Read/Write Directory Test");
            System.out.println("0: Exit");

            input = in.nextInt();

            resetDirectory(64);

            switch(input) {
                case 1:
                    addFilesTest();
                    break;
                case 2:
                    addFilesTest();
                    removeFilesTest();
                    break;
                case 3:
                    addFilesTest();
                    getInodeFromNameTest();
                    break;
                case 4:
                    readWriteTest();
                    break;
            }
        }
    }

    private void addFilesTest() {
        boolean passed = true;

        System.out.println("Adding Files to Directory...");

        short inodeNum;
        for (int i = 1; i < numInodes; i++) {
            inodeNum = dir.ialloc("f" + i);
            if (inodeNum < 0) {
                passed = false;
                System.out.println("Inode " + i + " FAILED: expected " + i + " but returned " + inodeNum);
            }
        }

        if (passed) {
            System.out.println("PASSED\n");
        }

        dir.printFileInfo();
    }

    private void removeFilesTest() {
        boolean passed = true;

        System.out.println("Removing Files from Directory...");

        for (short i = 1; i < numInodes; i++) {
            if (dir.ifree(i) == false) {
                passed = false;
                System.out.println("FAILED: failed to remove inode " + i);
            }
        }

        if (passed) {
            System.out.println("PASSED\n");
        }

        dir.printFileInfo();
    }

    private void getInodeFromNameTest() {
        boolean passed = true;

        System.out.println("Checking if we can receive inode by filename...");

        short inodeNum;
        for (int i = 1; i < numInodes; i++) {
            inodeNum = dir.namei("f" + i);
            if (inodeNum != i) {
                passed = false;
                System.out.println("Inode " + i + " FAILED: expected " + i + " but returned " + inodeNum);
            }
        }

        if (passed) {
            System.out.println("PASSED\n");
        }
    }

    private void readWriteTest() {

        // build dir and write dir to byte[]
        resetDirectory(numInodes);
        String name;
        for (int i = 1; i < numInodes; i++) {
            name = "";
            for (int j = 0; j < i % 30; j++) name += "a";
            dir.ialloc(name);
        }
        dir.printFileInfo();
        byte[] dirBytes = dir.directory2bytes();
        
        // create empty dir and load from byte[]
        resetDirectory(numInodes);
        dir.bytes2directory(dirBytes);
        dir.printFileInfo();
    }

    private void resetDirectory(int numInodes) {
        this.numInodes = numInodes;
        dir = new Directory(numInodes);
    }

    private void waitForInput() {
        System.out.println("Press enter to continue....");
        System.console().readLine();
    }
}