import java.io.*;
import java.util.Scanner;

class FileSystemTest {
    public static void main(String[] args) {

        FileSystem fs = new FileSystem();
        System.out.println("File System built");

        ///////////////////////////////////////////////////////
        // fs.write test
        ///////////////////////////////////////////////////////

        // # of blocks we want to fill
        float blocksToFill = 6.5f;

        // 11 blocks each containing 512 bytes
        byte[][] blocks = new byte[11][Disk.blockSize];
        byte[] dataToWrite = new byte[(int)blocksToFill * Disk.blockSize];


        // fill our data array with ints from 0 to x
        for (int i = 0; i < dataToWrite.length; i+=4) {
            SysLib.int2bytes(i, dataToWrite, i);
        }
        
        // make sure our ints were successfully converted to bytes
        // for (int i = 0; i < 100; i+=4) {
        //     System.out.println(SysLib.bytes2int(dataToWrite, i));
        // }

        //fs.writeTest(dataToWrite, blocks);
    }
}