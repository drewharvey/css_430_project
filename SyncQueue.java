//******************************************************************************
//**  File: SyncQueue.java
//**  Date: February 2014
//**  Author: Dimitar Dimitrov
//**  Description: SyncQueue node allows us to create a queue and allow 
//**  threads to wait for cetain condition/event to occur. In other words
//**  we allow parent threads to wait on their children before terminating.
//**  A child thread can wake up its parent via the dequeAndWakeUp function.
//**  Once woken up a parent/condition returns the tid of the child that has
//**  woken it up.
//******************************************************************************
import java.util.*;

public class SyncQueue extends Thread {
	private QueueNode[] queue;  // array of QueueNode objects
	private static final int SIZE = 10; // default size = 10
	
	// default constructor creates QueueNode array of size 10
	public SyncQueue() {
		queue = new QueueNode[SIZE]; 
		for ( int i = 0; i < SIZE; i++ )
	   	     queue[i] = new QueueNode();
	}
	
	// default constructor creates QueueNode array
	// of size - passed to the constructor
	public SyncQueue(int condMax) {
		queue = new QueueNode[condMax];
		for ( int i = 0; i < condMax; i++ )
	   		queue[i] = new QueueNode();
	}

	/*// MIGHT NEED TO Implement
	public void printQueueSize() {
		System.out.println(queue.getSize());

	} */ 

	// returns the ID of a child thread that has woken the calling thread
	public int enqueueAndSleep( int condition ){			
           return queue[condition].putToSleep(); 
	}

	// dequeueAndWakeup wakesUp a particular condition
	// this methods is called from Kernel when a child is terminating
	public void dequeueAndWakeup(int condition, int tid) {
		if(queue[condition] != null) {
			queue[condition].wakeUp(tid); 
		}	
	}
	
	// Overloaded dequeueAndWakeup function
	// if no second parameter given tid = 0
	public void dequeueAndWakeup(int condition) {
		if(queue[condition] != null) {
			queue[condition].wakeUp(0); // default tid=0
		}			
	} 
}
